using AuthenticationCenter.Utility;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//MiddleWare跨域
builder.Services.AddCors(option => option.AddPolicy("AllowCors", _build => _build.AllowAnyOrigin().AllowAnyMethod()));

//容器注册授权类
builder.Services.AddTransient<ICustomJWTService, CustomRssJWTService>();

//将配置文件的内容映射到类中
builder.Services.Configure<JWTTokenOption>(builder.Configuration.GetSection("JWTTokenOptions"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
{
    var forecast = Enumerable.Range(1, 5).Select(index =>
       new WeatherForecast
       (
           DateTime.Now.AddDays(index),
           Random.Shared.Next(-20, 55),
           summaries[Random.Shared.Next(summaries.Length)]
       ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast");

//允许跨域
//app.UseCors("AllowCors");
app.MapPost("/Authentication", (ICustomJWTService _iJWTService, string Name,string password) =>
{
    if ("xxx".Equals(Name) && "123456".Equals(password))//这里应该查数据库取得用户信息
    {
       
        int Sex = 1;
        string Role = "管理员";
        string token = _iJWTService.GetToken(Name, password, Sex, Role);
        return JsonConvert.SerializeObject(new
        {
            result = true,
            token
        });
    }
    else
    {
        return JsonConvert.SerializeObject(new
        {
            result = false,
            token = ""
        });
    }

});


app.Run();

internal record WeatherForecast(DateTime Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}