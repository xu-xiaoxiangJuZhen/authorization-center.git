﻿namespace AuthenticationCenter.Utility
{
    /// <summary>
    /// JWT配置文件映射用配置项
    /// </summary>
    public class JWTTokenOption
    {
        public string Audience
        {
            get;
            set;
        }
        public string SecurityKey
        {
            get;
            set;
        }
        //public SigningCredentials Credentials
        //{
        //    get;
        //    set;
        //}
        public string Issuer
        {
            get;
            set;
        }
    }
}
