﻿namespace AuthenticationCenter.Utility
{
    /// <summary>
    /// 定义一个接口用来处理 生成Token 
    /// </summary>
    public interface ICustomJWTService
    {
        string GetToken(string UserName, string NickName, int Sex, string Role);
    }
}
